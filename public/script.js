var playlist_id =null;
var currentId = -1;

var workoutName=null;

var trackIds;
var trackList = {};
var intervals = [];
var deviceID;
var _token;
var refreshCallback;
window.onSpotifyPlayerAPIReady = () => {
  //var playLists = getPlayLists();

  d3.json("/workouts", function(json){
    var fields = json.workouts.map(function(d){
      return d.name[0];
    });
    renderSelect("#workouts", fields, "")
    d3.selectAll('#workouts').on('change', function() {
      workoutName = this.value;
      if( playlist_id != null){
      try{
        d3.json("/matchTracks?playlist="+playlist_id+"&workout="+workoutName+"&token="+token, function(error, json){
          intervals.forEach(function(d){
            if( !d.track){
              throw new Error( "No track with tempo for "+d.cadence+" interval cadence, select another playlist.");

            }
          })
          intervals = json;
          intervalTable(intervals);
          startMusic(intervals, _token);
        })
      }catch( e ){
        alert( e );
      }
    }
    });
  })

}
// Play a specified track on the Web Playback SDK's device ID
function play(device_id, spotify_uri) {
//  console.log("spotify_uri: " + spotify_uri);
    let index = currentId;
    d3.request("https://api.spotify.com/v1/me/player/play?device_id=" + device_id)
    .header("Content-Type", "application/json")
    .header("Authorization", 'Bearer ' + _token)
    .send("PUT",
    JSON.stringify({ uris: [spotify_uri], position_ms:5000 }), function(error, d ){
      if( error){
        console.log( error );
        // if it's a 401 error, try refreshing the token and calling play again
      }
      refreshCallback();

      var row = d3.select(".ID"+(index));
      row.classed("table-success", true);
      $(".ID"+(index)).get(0).scrollIntoView();
      console.log( d );
    })
}
function setTokenCallback(refreshToken){
  refreshCallback =refreshToken ;
}

function updateToken(access_token){
  _token = access_token;
}

//
function getPlayLists(token) {
  var playlists = [];

  d3.json( "https://api.spotify.com/v1/me/playlists").header("Authorization", 'Bearer ' + token)
  .get(function( error, json ){

      json.items.forEach(function(d) {
        playlists.push( d );
      })

      renderPSelect("#playlists", playlists, "")
      d3.selectAll('#playlists').on('change', function(){
        playlist_id = this.value
        if( workoutName != null){

            d3.json("/matchTracks?playlist="+playlist_id+"&workout="+workoutName+"&token="+token, function(error, json){
              intervals = json;
              try{
                intervals.forEach(function(d){
                  if( !d.track){
                    throw new Error( "No track with tempo for "+d.cadence+" interval cadence, select another playlist.");
                  }
                })
                intervalTable(intervals);
                startMusic(intervals, token);
              }catch( e ){
                alert( e );
              }
            })
        }
      });
    });
    return playlists;
}

function startMusic(intervals, token ) {
  _token = token;
  const player = new Spotify.Player({
    name: 'ZMusic Interval Player',
    getOAuthToken: cb => {
      cb(_token);
    }
  });

  // Error handling
  player.on('initialization_error', e => console.error(e));
  player.on('authentication_error', function(e){
    console.error(e);
    refreshCallback();
  });
  player.on('account_error', e => console.error(e));
  player.on('playback_error', e => console.error(e));

  // Playback status updates
  player.on('player_state_changed', state => {
    console.log(state)
    if( typeof(state) != "undefined" && state != null ){
      $('#current-track').attr('src', state.track_window.current_track.album.images[0].url);
      $('#current-track-name').text(state.track_window.current_track.name);
    }
  });

  // Ready
  player.on('ready', data => {
    console.log('Ready with Device ID', data.device_id);
    deviceID = data.device_id;
    // Play a track using our new device ID
  });

  // Connect to the player!
  player.connect();
}
//timeout methods
var interval_timeout;
var extra_timeout; //if the interval is longer than the song
function playNext() {
  clearTimeout(interval_timeout);
  clearTimeout(extra_timeout);
  currentId++;

  var track_id = intervals[currentId].track.uri;
  play(deviceID, track_id);
  var duration_diff = intervals[currentId].duration - intervals[currentId].track.features.duration_ms/1000;
  if( duration_diff > 0 ){
    let timeout_time = intervals[currentId].track.features.duration_ms;
    extra_timeout = setTimeout( playExtra, timeout_time  );
  }
  if (currentId <= intervals.length)
    interval_timeout = setTimeout(playNext, intervals[currentId].duration * 1000);
}
//keep playing the song until it's replaced by the next interval trqck
function playExtra() {
  clearTimeout(extra_timeout);
  play(deviceID, intervals[currentId].track.uri);
  extra_timeout = setTimeout(playExtra, intervals[currentId].track.features.duration_ms);
}

function pauseMusic(){
  clearTimeout(interval_timeout);
  clearTimeout(extra_timeout);

  d3.request("https://api.spotify.com/v1/me/player/pause")
  .header("Content-Type", "application/json")
  .header("Authorization", 'Bearer ' + _token)
  .send("PUT", function( d ){
    console.log( d );
  })
}

function   renderSelect(id, fields, value){
  d3.select(id).selectAll("option").data(fields).enter().append("option")
    .text(function(d){return d;})
  d3.select(id).property("selectedIndex", fields.indexOf(value));
}
function   renderPSelect(id, fields, value){
  d3.select(id).selectAll("option").data(fields).enter().append("option")
  .attr("value",function(d){
    return d.id;
  })
    .text(function(d){
      return d.name;
    })
  d3.select(id).property("selectedIndex", fields.indexOf(value));
}


function intervalTable(data){
  var table = d3.select("#table").append("table").attr("class","table");
        var header = table.append("thead").append("tr");
        header
                .selectAll("th")
                .data(["Time(s)", "Power", "Cadence", "Options","Tempo", "Track"])
                .enter()
                .append("th")
                .text(function(d) { return d; });
        var tablebody = table.append("tbody");
        rows = tablebody
                .selectAll("tr")
                .data(data)
                .enter()
                .append("tr")
                .attr("class",function(d, i){
                  return "ID"+i;
                });
        // We built the rows using the nested array - now each row has its own array.
        cells = rows.selectAll("td")
            // each row has data associated; we get it and enter it for the cells.
                .data(function(d) {
                    return [d.duration, d.power, d.cadence, d.tracks.length, d.track.features.tempo, d.track.name];
                })
                .enter()
                .append("td")
                .text(function(d) {
                    return d;
                });
}
