var workouts;
const attribute = "energy";

var powerScale = d3.scaleLinear();

function readWorkouts( callback ) {
  var fields = [];
  workouts = {};
  d3.xml("workouts.xml", function(data) {
    fields = [].map.call(data.querySelectorAll("workout_file"), function(workout_file) {
      var name = workout_file.querySelector("name").textContent;
      workouts[name] = workout_file;
      return name;
    });
  })
}

function matchTracks() {
  intervals = [];

  var workout = workouts[workoutName];
  var preintervals = [].map.call(workout.querySelectorAll("Warmup, IntervalsT, SteadyState, Ramp, Cooldown, Freeride"), function(warmup) {
    return {
      repeat: Number(warmup.getAttribute("Repeat")),
      duration: Number(warmup.getAttribute("Duration")),
      power: Number(warmup.getAttribute("Power")),
      onDuration: Number(warmup.getAttribute("OnDuration")),
      offDuration: Number(warmup.getAttribute("OffDuration")),
      powerLow: Number(warmup.getAttribute("PowerLow")),
      powerHigh: Number(warmup.getAttribute("powerHigh")),
      cadenceResting: Number(warmup.getAttribute("CadenceResting")),
      cadence: Number(warmup.getAttribute("Cadence")),
      onPower: Number(warmup.getAttribute("OnPower")),
      offPower: Number(warmup.getAttribute("OffPower")),
    };
  });
  preintervals.forEach(function(d) {
    var repeat = Math.max(1, d.repeat);
    if( d.onDuration > 0 )
      repeat *= 2;
    for (var i = 0; i < repeat; i++) {
      var interval = {};

      interval.duration = getDuration(d, i);
      interval.power = getPower(d, i);
      interval.cadence = getCadence(d, i);

      intervals.push(interval);
    }
  })
  powerScale.range(d3.extent(intervals, function(i) {
    return i.power;
  }));

  intervals.forEach(function(interval) {
    interval.tracks = matchingTempo(interval, trackList);
    if( interval.tracks.length == 0){
      throw new Error( "No track with tempo for "+interval.cadence+" interval cadence, select another playlist.");
    }
  })
  var cadenceGroups = {};
  intervals.forEach(function(interval) {
    let cadence = interval.cadence;
    if (!cadenceGroups[cadence]) {
      cadenceGroups[cadence] = [];
    }
    cadenceGroups[cadence].push(interval);
  })

  intervals.forEach(function(interval) {
    console.log( "I duration = "+interval.duration);

    if( interval.duration >= 500){
      trackList.forEach( function(t){
        console.log( "track duration = "+t.duration );

        if( t.duration > 500 ){
          interval.track = longTrack();
        }
      })
    }else{
      interval.track = bestTrack(interval, cadenceGroups[interval.cadence]);
      interval.track.used = true;
    }
  })

  return intervals;
}

function getDuration(d, i) {
  let duration = d.duration;
  if (d.onDuration > 0) {
    if (i % 2 == 0) {
      return d.onDuration;
    } else {
      return d.offDuration;
    }
  }
  return duration;
}

function getPower(d, i) {
  if (d.power > 0) return d.power;
  if (d.onPower > 0) {
    if (i % 2 == 0) {
      return d.onPower;
    } else {
      return d.offPower;
    }
  }
  return 0.5;
}

function getCadence(d, i) {

  let cadence = d.cadence;
  if (cadence == 0) cadence = 90;
  if (d.cadenceResting > 0) {
    if (i % 2 == 0) {
      return d.cadence;
    } else {
      return d.cadenceResting;
    }
  }
  return cadence;
}
function bestTrack( interval, cadenceGroup){
  var best = interval.tracks[0];
  var match = 1000;
  powerScale.domain( d3.extent( interval.tracks, function(t){
    return t.features[attribute];
  }))
  powerScale.range(d3.extent(Object.values(cadenceGroup), function(i){
    return i.power;
  }));
  let attributeValue = powerScale.invert( interval.power);

  interval.tracks.forEach(function(t) {
    if( t != null && t != cadenceGroup.last){
      let diff = Math.abs( t.features[attribute] - attributeValue);

       if (diff < match) {
          best = t;
          match = diff;
      }
    }
  })
  cadenceGroup.last = best;
  return best;
}

function matchingTempo( interval, trackList){
  tracks = [];
  Object.values(trackList).forEach(function(t) {
      if( t != null ){
        let diff = Math.abs(t.features.tempo  - interval.cadence)
        diff = Math.min( diff, Math.abs(t.features.tempo / 2 - interval.cadence))
        if( diff < 2 ){
          tracks.push(t);
        }
      }
  })
  return tracks;
}

function getPlayListInformation(playlist_id, offset) {
  d3.json( "https://api.spotify.com/v1/playlists/" + playlist_id + "/tracks?offset="+offset)
  .header("Authorization", 'Bearer ' + _token)
  .get(function( json ){
      console.log(json)
      trackIds = "ids=";
      json.items.forEach(function(d) {
        trackIds += d.track.id + ",";
        trackList[d.track.id] = d.track;
      })
      getSpotifyAudioFeatures(trackIds, trackList);
      if( json.offset+json.limit < json.total){
          getPlayListInformation(playlist_id, offset+1000);
      }
    });
}

function getSpotifyAudioFeatures(trackIds){

  d3.json(  "https://api.spotify.com/v1/audio-features/?" + trackIds)
  .header("Authorization", 'Bearer ' + _token)
  .get(function( data ){
       data.audio_features.forEach( function(af){
         if( af )
          trackList[af.id].features = af;
      });

      Histogram.draw_histogram(trackList, "tempo");
      Histogram.draw_histogram(trackList, "energy");
  });
}
