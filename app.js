var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var request = require('request'); // "Request" library
var cors = require('cors');
var querystring = require('querystring');
var fs           = require('fs');

var xml2js       = require('xml2js');
var parser       = new xml2js.Parser();
var DOMParser = require('xmldom').DOMParser;
var d3       = require('d3');
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var client_id = '4a0eebaf6e1e49f58665a8eb4e3744a6'; // Your client id
var client_secret = 'bcf9e551c42946f590b426f990042540'; // Your secret
var redirect_uri = 'http://zmusic-zwiftmusic.7e14.starter-us-west-2.openshiftapps.com/callback/'; // Your redirect uri

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(cors())

app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
//app.use(function(req, res, next) {
//  next(createError(404));
//});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});
app.get('/hello', function (req, res) {
  res.send('Hello World!')
})
/**
 * Generates a random string containing numbers and letters
 * @param  {number} length The length of the string
 * @return {string} The generated string
 */

var generateRandomString = function(length) {
  var text = '';
  var possible = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

  for (var i = 0; i < length; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length));
  }
  return text;
};

var stateKey = 'spotify_auth_state';
app.get('/login', function(req, res) {

  var state = generateRandomString(16);
  res.cookie(stateKey, state);

  // your application requests authorization
  var scope = 'user-read-private user-read-email streaming user-modify-playback-state';
  res.redirect('https://accounts.spotify.com/authorize?' +
    querystring.stringify({
      response_type: 'code',
      client_id: client_id,
      scope: scope,
      redirect_uri: redirect_uri,
      state: state
    }));
});

app.get('/callback', function(req, res) {

  // your application requests refresh and access tokens
  // after checking the state parameter

  var code = req.query.code || null;
  var state = req.query.state || null;
  var storedState = req.cookies ? req.cookies[stateKey] : null;

  if (state === null || state !== storedState) {
    res.redirect('/#' +
      querystring.stringify({
        error: 'state_mismatch'
      }));
  } else {
    res.clearCookie(stateKey);
    var authOptions = {
      url: 'https://accounts.spotify.com/api/token',
      form: {
        code: code,
        redirect_uri: redirect_uri,
        grant_type: 'authorization_code'
      },
      headers: {
        'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64'))
      },
      json: true
    };

    request.post(authOptions, function(error, response, body) {
      if (!error && response.statusCode === 200) {

        var access_token = body.access_token,
            refresh_token = body.refresh_token;

        var options = {
          url: 'https://api.spotify.com/v1/me',
          headers: { 'Authorization': 'Bearer ' + access_token },
          json: true
        };

        // use the access token to access the Spotify Web API
        request.get(options, function(error, response, body) {
          console.log(body);
        });

        // we can also pass the token to the browser to make requests from there
        res.redirect('/#' +
          querystring.stringify({
            access_token: access_token,
            refresh_token: refresh_token
          }));
      } else {
        res.redirect('/#' +
          querystring.stringify({
            error: 'invalid_token'
          }));
      }
    });
  }
});

app.get('/refresh_token', function(req, res) {

  // requesting access token from refresh token
  var refresh_token = req.query.refresh_token;
  var authOptions = {
    url: 'https://accounts.spotify.com/api/token',
    headers: { 'Authorization': 'Basic ' + (new Buffer(client_id + ':' + client_secret).toString('base64')) },
    form: {
      grant_type: 'refresh_token',
      refresh_token: refresh_token
    },
    json: true
  };

  request.post(authOptions, function(error, response, body) {
    if (!error && response.statusCode === 200) {
      var access_token = body.access_token;
      res.send({
        'access_token': access_token
      });
    }
  });
});
app.get('/workouts', function(req, res, next) {

    var xmlfile = __dirname + "/public/workouts.xml";

    fs.readFile(xmlfile, "utf-8", function (error, text) {
        if (error) {
            throw error;
        }else {

            parser.parseString(text, function (err, result) {
                var name = result['all']['workout_file'];
                res.send({ workouts:  name });
            });
        }
   });
});

app.get('/matchTracks', function(req, res) {
    let playlist = req.query.playlist;
    let workout = req.query.workout;
    let token = req.query.token;
    console.log("playlist: "+playlist);
    var xmlfile = __dirname + "/public/workouts.xml";
    var trackList = [];
    fs.readFile(xmlfile, "utf-8", function (error, text) {
        if (error) {
          throw error;
        }else {
          var parser = new DOMParser();
          var xmlDoc = parser.parseFromString(text, "text/xml");
          getPlayListInformation(playlist, 0, token, trackList,
            function(tracks){
              console.log( "# of tracks: "+tracks.length);
              var intervals = matchTracks( xmlDoc.documentElement, workout, tracks)
              console.log( "sending: "+intervals)
              res.send(intervals)
            });
        }
   });

});
const attribute = "energy";

function matchTracks(data, workoutName, trackList) {
   intervals = [];
   console.log("workoutName "+workoutName);

   var preintervals = [];
   Array.prototype.slice.call(data.getElementsByTagName("workout_file")).forEach(function(w){
      let name = w.getElementsByTagName("name")[0].childNodes[0].nodeValue;
        if( name == workoutName){
          Array.prototype.slice.call(w.getElementsByTagName("workout")[0].childNodes).forEach(function(element){
            if( element.localName != null){
              let interval = {};
              for (i = 0; i < element.attributes.length; i++) {
                  interval[element.attributes[i].name] = Number(element.attributes[i].value);
                }
                preintervals.push( interval );
              }
          });
        }
    });

  preintervals.forEach(function(d) {
    var repeat = 1;

    if( !isNaN(d.Repeat))
       repeat = Math.max(1, d.Repeat);

    if( !isNaN(d.OnDuration) && d.OnDuration > 0 )
      repeat *= 2;
    for (var i = 0; i < repeat; i++) {
      var interval = {};

      interval.duration = getDuration(d, i);
      interval.power = getPower(d, i);
      interval.cadence = getCadence(d, i);
      intervals.push(interval);
    }
  })

  var powerScale = d3.scaleLinear();

  powerScale.range(d3.extent(intervals, function(i) {
    return i.power;
  }));

  intervals.forEach(function(interval) {
    interval.tracks = matchingTempo(interval, trackList);
    if( interval.tracks.length == 0){
      console.log(trackList.length+ " No track with tempo for "+interval.cadence+" interval cadence, select another playlist." )
    //  throw new Error( "No track with tempo for "+interval.cadence+" interval cadence, select another playlist.");
    }
  })
  var cadenceGroups = {};
  intervals.forEach(function(interval) {
    let cadence = interval.cadence;
    if (!cadenceGroups[cadence]) {
      cadenceGroups[cadence] = [];
    }
    cadenceGroups[cadence].push(interval);
  })

  intervals.forEach(function(interval) {
    console.log( "I duration = "+interval.duration);

    if( interval.duration >= 500){
      Object.values(trackList).forEach(function(t) {
          if( t != null ){
            console.log( "T duration = "+t.features.duration_ms/1000);

            if(t.features.duration_ms/1000 > 500 ){
              interval.track = t;
            }
          }
      })
    }else{
      if( cadenceGroups[interval.cadence].length > 0){
        interval.track = bestTrack(interval, cadenceGroups[interval.cadence], powerScale);
        if(   interval.track )
          interval.track.used = true;
        }
      }
  })

  return intervals;
}

function getDuration(d, i) {
  let duration = d.Duration;
  if (d.OnDuration > 0) {
    if (i % 2 == 0) {
      return d.OnDuration;
    } else {
      return d.OffDuration;
    }
  }
  return duration;
}

function getPower(d, i) {
  if (d.Power > 0) return d.Power;
  if (d.OnPower > 0) {
    if (i % 2 == 0) {
      return d.OnPower;
    } else {
      return d.OffPower;
    }
  }
  return 0.5;
}

function getCadence(d, i) {

  let cadence = 90;
  if( !isNaN( d.Cadence))
    cadence = Number(d.Cadence)

  if (cadence == 0) cadence = 90;
  if (d.CadenceResting > 0) {
    if (i % 2 == 0) {
      return d.Cadence;
    } else {
      return d.CadenceResting;
    }
  }
  return cadence;
}
function bestTrack( interval, cadenceGroup, powerScale){
  var best = interval.tracks[0];
  var match = 1000;
  powerScale.domain( d3.extent( interval.tracks, function(t){
    return t.features[attribute];
  }))
  powerScale.range(d3.extent(cadenceGroup, function(i){
    return i.power;
  }));
  let attributeValue = powerScale.invert( interval.power);

  shuffle(interval.tracks).forEach(function(t) {
    if( t != null && t != cadenceGroup.last){
      let diff = Math.abs( t.features[attribute] - attributeValue);
      if( t.features.duration_ms/1000 > interval.duration+5){
        diff /= 5.0;
      }
      if (diff < match) {
          console.log("better diff "+diff+" = tr:"+t.features[attribute] +" - in:"+ attributeValue)
          best = t;
          match = diff;
      }
    }
  })

  cadenceGroup.last = best;
  return best;
}

function matchingTempo( interval, trackList){
  tracks = [];
  Object.values(trackList).forEach(function(t) {
      if( t != null ){
        let diff = Math.abs(t.features.tempo  - interval.cadence)
        diff = Math.min( diff, Math.abs(t.features.tempo / 2 - interval.cadence))
        if( diff < 2 ){
          tracks.push(t);
        }
      }
  })
  return tracks;
}

function getPlayListInformation(playlist_id, offset, _token, trackList, callback) {
 console.log(" playlist info "+offset);
  d3.json( "https://api.spotify.com/v1/playlists/" + playlist_id + "/tracks?offset="+offset)
  .header("Authorization", 'Bearer ' + _token)
  .get(function( json ){
      let trackIds = "ids=";
      json.items.forEach(function(d) {
        trackIds += d.track.id + ",";
        trackList[d.track.id] = d.track;
      })
      getSpotifyAudioFeatures(trackIds, trackList, _token, function(){
        if( json.offset+json.limit < json.total){
            getPlayListInformation(playlist_id, offset+100, _token, trackList, callback);
        }else{
          callback(trackList);
        }
      });
    });
    return trackList;
}

function getSpotifyAudioFeatures(trackIds, trackList, token, callback){
  d3.json(  "https://api.spotify.com/v1/audio-features/?" + trackIds)
  .header("Authorization", 'Bearer ' + token)
  .get(function( error, data ){

       data.audio_features.forEach( function(af){
         if( af )
          trackList[af.id].features = af;
      });
      callback();
    //  Histogram.draw_histogram(trackList, "tempo");
    //  Histogram.draw_histogram(trackList, "energy");
  });
}

function shuffle(array) {
  var currentIndex = array.length, temporaryValue, randomIndex;

  // While there remain elements to shuffle...
  while (0 !== currentIndex) {

    // Pick a remaining element...
    randomIndex = Math.floor(Math.random() * currentIndex);
    currentIndex -= 1;

    // And swap it with the current element.
    temporaryValue = array[currentIndex];
    array[currentIndex] = array[randomIndex];
    array[randomIndex] = temporaryValue;
  }

  return array;
}

module.exports = app;
